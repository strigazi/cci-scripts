#!/bin/bash

source /var/lib/rundeck/data/openrc

KO=0
IRONIC_PROD_PROJECT='Cloud Test - gva_baremetal_001'
echo "[INFO] Looking for Ironic nodes to $RD_OPTION_MAINTENANCE maintenance..."

# remove .cern.ch and lowercase all instances passed
NORMALIZED_INSTANCES=`echo $RD_OPTION_INSTANCES | sed 's/.cern.ch//g' | awk '{print tolower($0)}'`
for INSTANCE in $NORMALIZED_INSTANCES
do
    INSTANCE_ID=$(openstack server list --all-projects --name $INSTANCE -n | grep $INSTANCE | awk '{print $2}')

    if [[ -z "${INSTANCE_ID}" ]]; then
      echo "[INFO] No project found for instance $INSTANCE. This not an Ironic-managed node!. Moving on to the next node..."
      continue
    else
      echo "[INFO] Ironic instance $INSTANCE was found. Collecting baremetal node information..."
      IRONIC_NODE_UUID=$(OS_PROJECT_NAME=$IRONIC_PROD_PROJECT OS_BAREMETAL_API_VERSION=1.9 openstack baremetal node show --instance $INSTANCE_ID -f value -c uuid)
      echo "[INFO] Ironic instance $INSTANCE belongs to baremetal node '$IRONIC_NODE_UUID'."
    fi

    if [ ${RD_OPTION_BEHAVIOUR} == 'perform' ]; then
      echo "[INFO] Calling maintenance $RD_OPTION_MAINTENANCE for node '$IRONIC_NODE_UUID'..."
      if [ ${RD_OPTION_MAINTENANCE} == 'set' ]; then
        EXIT_CODE=$(OS_PROJECT_NAME=$IRONIC_PROD_PROJECT openstack baremetal node maintenance $RD_OPTION_MAINTENANCE --reason "$RD_OPTION_MAINTENANCE maintenance triggered by $RD_JOB_USERNAME using Rundeck ($RD_JOB_ID)" $IRONIC_NODE_UUID)
      else
        EXIT_CODE=$(OS_PROJECT_NAME=$IRONIC_PROD_PROJECT openstack baremetal node maintenance $RD_OPTION_MAINTENANCE $IRONIC_NODE_UUID)
      fi

      if [[ $EXIT_CODE -ne 0 ]]; then
        ((KO++))
      else
        echo "[INFO] Maintenance was updated successfully!"
      fi

    else
      echo "[DRYRUN] Calling maintenance $RD_OPTION_MAINTENANCE for Ironic node '$IRONIC_NODE_UUID'..."
    fi

done

if [[ $K0 -ne 0 ]]; then
  echo "[ERROR] $RD_OPTION_MAINTENANCE failed for some Ironic nodes. Please check logs"
  exit 1
fi
