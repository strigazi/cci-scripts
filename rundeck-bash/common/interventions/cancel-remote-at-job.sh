#!/bin/bash

for HOST in $RD_OPTION_HOSTS
  do
    echo "[INFO] Cancelling Rundeck managed AT jobs on host $HOST..."
    OUT=$(ssh -q -o StrictHostKeyChecking=no root@$HOST "at -l" 2>&1)
    echo "$OUT"
    for JOBID in $(echo "$OUT" | awk {'print $1'});
      do
        JOB_INFO=$(ssh -q -o StrictHostKeyChecking=no root@$HOST "at -c $JOBID" 2>&1)
        echo $JOB_INFO | grep --silent "managed_by_rundeck"
        if [[ $? -eq 0 ]]; then
            echo "[INFO] Deleting AT job with ID $JOBID..."
            OUT=$(ssh -q -o StrictHostKeyChecking=no root@$HOST "atrm $JOBID" 2>&1)
        else
            echo "[WARNING] AT job with ID $JOBID is not managed by Rundeck. Nothing to do."
        fi
    done
    echo
  done
