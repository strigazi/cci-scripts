#!/bin/bash

if [[ ${RD_OPTION_REFERENCE} == "" ]]; then
  REASON="${RD_OPTION_STATUS^}d by $RD_JOB_USER_NAME using Rundeck ($RD_JOB_EXECID)"
else
  REASON="[$RD_OPTION_REFERENCE] ${RD_OPTION_STATUS^}d by $RD_JOB_USER_NAME using Rundeck ($RD_JOB_EXECID)"
fi

echo "[INFO] Using roger to $RD_OPTION_STATUS alarms on the following hosts: $RD_OPTION_HOSTS"

ROGER_OPTIONS=""
if [ ! -z "$RD_OPTION_DURATION" ]; then
  echo "[INFO] Roger request will be active in $RD_OPTION_DURATION hours."
  ROGER_OPTIONS+="--duration ${RD_OPTION_DURATION}h"
fi

if [ ${RD_OPTION_BEHAVIOUR} == 'perform' ]; then
  echo "[INFO] Trying to $RD_OPTION_STATUS roger alarms on $RD_OPTION_HOSTS..."
  if [ ${RD_OPTION_STATUS} == 'disable' ]; then
    echo "[INFO] Node scheduled for deletion"
    echo $RD_OPTION_HOSTS | xargs -n 20 roger update --all_alarms false --message "$REASON" $ROGER_OPTIONS
  else
    echo $RD_OPTION_HOSTS | xargs -n 20 roger update --all_alarms true --message "$REASON" $ROGER_OPTIONS
  fi

  if [ $? -eq 0 ]; then
    echo "[INFO] Roger alarms sucessfully "$RD_OPTION_STATUS"d on $RD_OPTION_HOSTS."
  else
    "[ERROR] Failed to "$RD_OPTION_STATUS"d roger on $RD_OPTION_HOSTS."
  fi

  sleep 1 # sync output messages
  echo "[INFO] Showing alarms status information per host..."
  echo $RD_OPTION_HOSTS | xargs -n 20 roger show
else
  if [ ${RD_OPTION_STATUS} == 'disable' ]; then
    echo $RD_OPTION_HOSTS | xargs -n 20 echo "[DRYRUN][INFO] roger update --all_alarms false --message \"$REASON\" $ROGER_OPTIONS"
  else
    echo $RD_OPTION_HOSTS | xargs -n 20 echo "[DRYRUN][INFO] roger update --all_alarms true --message \"$REASON\" $ROGER_OPTIONS"
  fi
fi
