#!/bin/bash

source /var/lib/rundeck/data/openrc

OK=0
KO=0

#only execute this once since it takes reallyyy long
NOVA_SERVICE_LIST="$(nova service-list)"

if [[ ${RD_OPTION_REFERENCE} == "" ]]; then
  REASON_MESSAGE="${RD_OPTION_STATUS^}d by $RD_JOB_USER_NAME using Rundeck ($RD_JOB_EXECID)"
else
  REASON_MESSAGE="[$RD_OPTION_REFERENCE] ${RD_OPTION_STATUS^}d by $RD_JOB_USER_NAME using Rundeck ($RD_JOB_EXECID)"
fi

if [ ! -z "$NOVA_SERVICE_LIST" ]; then
  for HOST in $RD_OPTION_HOSTS
    do
      # Gettting cell info
      HOST_LOWER=${HOST,,} #lower case hostname
      HOST_INFO=$(echo "$NOVA_SERVICE_LIST" | awk "/$HOST_LOWER/ && /nova-compute/" | awk '{print $2}')

      if [ ! -z "$HOST_INFO" ]; then
        echo "[INFO] Trying to $RD_OPTION_STATUS nova-compute on $HOST_INFO..."
        if [ ${RD_OPTION_BEHAVIOUR} == 'perform' ]; then
          if [ ${RD_OPTION_STATUS} == 'disable' ]; then
            echo "[INFO] Node scheduled for disabling of nova-compute."
            nova service-$RD_OPTION_STATUS $HOST_INFO --reason "$REASON_MESSAGE"
          else
            nova service-$RD_OPTION_STATUS $HOST_INFO
          fi
          if [ $? -eq 0 ]; then
            echo "[INFO] nova-compute sucessfully "$RD_OPTION_STATUS"d on $HOST."
            ((OK++))
          else
            sleep 1 # output messages order
            echo "[ERROR] Failed to "$RD_OPTION_STATUS" nova-compute on $HOST."
            ((KO++))
          fi
        else
          echo "[DRYRUN][INFO] nova service-$RD_OPTION_STATUS $HOST_INFO --reason \""$REASON_MESSAGE"\""
          ((OK++))
        fi
      else
        echo "[ERROR] Not found. Has $HOST been deleted already?"
        ((KO++))
      fi
      echo ""
    done

else
  echo "[ERROR] Something went wrong executing nova service-list. Please check with the OpenStack admins."
  exit 2
fi

#Summary
printf "\n:TOTAL HOSTS:SUCCESS:ERROR\n[${RD_OPTION_BEHAVIOUR^^}] SUMMARY NOVA ${RD_OPTION_STATUS^^}:     $((OK + KO)):   $OK:  $KO" | column  -t -s ':'
echo ""

if [ $KO -gt 0 ]; then
  echo "[ERROR] nova service-$RD_OPTION_STATUS did not work for some hosts. Please check logs."
  exit 2
fi
