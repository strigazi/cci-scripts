#!/bin/bash

source /var/lib/rundeck/data/openrc

OK=0
KO=0

#only execute this once since it takes reallyyy long
NOVA_SERVICE_LIST="$(nova service-list)"

echo "[INFO] Trying to remove the following compute nodes from Nova: $RD_OPTION_HOSTS..."

if [ ! -z "$NOVA_SERVICE_LIST" ]; then
  for HOST in $RD_OPTION_HOSTS
    do
      # Gettting cell info
      HOST_LOWER=${HOST,,} #lower case hostname
      HOST_ID=$(echo "$NOVA_SERVICE_LIST" | awk "/$HOST_LOWER/ && /nova-compute/" | awk '{print $2}')

      if [ ! -z "$HOST_ID" ]; then
        echo "[INFO] Removing compute node $HOST from Nova..."
        if [ ${RD_OPTION_BEHAVIOUR} == 'perform' ]; then
          echo "[INFO] Executing: nova service-delete $HOST_ID..."

          nova service-delete "$HOST_ID"
          if [ $? -eq 0 ]; then
            echo "[INFO] $HOST sucessfully deleted"
            ((OK++))
          else
            sleep 1 #log messages sync
            echo "[ERROR] Failed to delete compute node on from Nova"
            ((KO++))
          fi
        else
          echo "[DRYRUN] Would've removed compute node $HOST from Nova"
          ((OK++))
        fi
      else
        echo "[ERROR] Not found. Is $HOST a hypervisor?"
        ((KO++))
      fi
      echo ""
    done

else
  echo "[ERROR] Something went wrong executing nova service-list. Please check with the OpenStack admins."
  exit 2
fi

#Summary
printf "\n:TOTAL HOSTS:SUCCESS:ERROR\n[${RD_OPTION_BEHAVIOUR^^}] SUMMARY SERVICE DELETE:     $(($OK + $KO)):   $OK:  $KO" | column  -t -s ':'
echo ""

if [ $KO -gt 0 ]; then
  echo "[ERROR] Not possible to remove some compute nodes from Nova. Please check logs."
  exit 2
fi
