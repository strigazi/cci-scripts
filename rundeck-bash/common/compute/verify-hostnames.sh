#!/bin/bash

for HOST in $RD_OPTION_HOSTS
  do
    #append .cern.ch
    if [[ $HOST != *.cern.ch ]]; then
      HOST="$HOST.cern.ch"
    fi
    #isolate hostgroup name
    HOSTGROUP=$(ai-foreman -f "name=${HOST,,}" --no-header -z Hostgroup showhost 2> /dev/null | awk {'print $2'} | xargs)
    echo $HOSTGROUP
    if [[ $HOSTGROUP != "" ]]; then
      if [[ $HOSTGROUP != "cloud_"* ]]; then
           echo "[ERROR] Hostname '$HOST' does not belong to any of the 'Cloud Infrastructure' hostgroups. This Rundeck job can only work with Cloud hosts."
           exit 2
      fi
    else
      echo "[ERROR] Hostname '$HOST' not found on Foreman. Please verify if this host belongs to 'ai-openstack-admin' before running it again."
      exit 2
    fi
  done
