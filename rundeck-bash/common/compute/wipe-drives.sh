#!/bin/bash

SUFIX=".cern.ch"

if [ ${RD_OPTION_BEHAVIOUR} == 'perform' ]; then
 echo "INFO:rundeck:Executing aims2client to wipe disk during next startup on: $RD_OPTION_HOSTS..."
 for HOST in $RD_OPTION_HOSTS
   do
     # un-fqdn hostname
     if [[ $HOST == *.cern.ch ]]; then
       HOST=${HOST%$SUFIX}
     fi
     echo "Using aims2client to wipe disk of $HOST..."
     aims2client addhost $HOST --kopts "load_ramdisk=1 root=/dev/ram rw console=ttyS0,9600 console=ttyS1,9600 console=tty0 runscript=deathdisk_bash.sh runarg=keep-creds" --pxe --name U-SLC6.7-2-X86_64
   done
else
echo "[DRYRUN] Would have wiped $RD_OPTION_HOSTS disks"
fi

echo ""
