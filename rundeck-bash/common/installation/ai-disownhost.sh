#!/bin/bash

source /var/lib/rundeck/data/openrc

echo "INFO:rundeck:Executing ai-disownhost for the following hosts $RD_OPTION_HOSTS..."

if [ ${RD_OPTION_BEHAVIOUR} == 'perform' ]; then
 ai-disownhost --roger-disable --owner $RD_OPTION_OWNER --hostgroup $RD_OPTION_HOSTGROUP --landb-pass $OS_PASSWORD $RD_OPTION_HOSTS
else
 ai-disownhost --dryrun --roger-disable --owner $RD_OPTION_OWNER --hostgroup $RD_OPTION_HOSTGROUP --landb-pass $OS_PASSWORD $RD_OPTION_HOSTS
fi

if [[ $? -ne 0 ]]; then
 echo "ERROR:rundeck:ai-disownhost did not finish successfully"
 exit 1
fi
