#!/bin/bash

echo "[INFO] Executing ai-foreman updatehost over the following hosts $RD_OPTION_HOSTS..."

if [ ${RD_OPTION_BEHAVIOUR} == 'perform' ]; then
#      --hostgroup       $RD_OPTION_INCOMING_HOSTGROUP \
  ai-foreman updatehost --after \
      --environment     "$RD_OPTION_ENVIRONMENT" \
      --operatingsystem "$RD_OPTION_OPERATINGSYSTEM" \
      --medium          "$RD_OPTION_MEDIUM" \
      --architecture    "$RD_OPTION_ARCHITECTURE" \
      --ptable          "$RD_OPTION_PTABLE" \
      $RD_OPTION_HOSTS

  if [[ $? -ne 0 ]]; then
    echo "[ERROR] ai-foreman updatehost did not finish successfully. Please check summary table"
    exit -1
  fi

else
  echo "[DRYRUN] Executing ai-foreman updatehost in dryrun mode"
  ai-foreman updatehost \
      --dryrun \
      --environment     "$RD_OPTION_ENVIRONMENT" \
      --operatingsystem "$RD_OPTION_OPERATINGSYSTEM" \
      --medium          "$RD_OPTION_MEDIUM" \
      --architecture    "$RD_OPTION_ARCHITECTURE" \
      --ptable          "$RD_OPTION_PTABLE" \
      $RD_OPTION_HOSTS
fi
