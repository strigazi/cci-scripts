FROM cern/cc7-base:latest
MAINTAINER Mateusz Kowalski <mateusz.kowalski@cern.ch>

# Fix for corrupted RPMDB
RUN yum install -y yum-plugin-ovl

# Add all Openstack related repositories (upstream and rebuilds)
RUN printf '[cci7-iaas-queens] \nname=Cern rebuilds for IaaS Openstack \nbaseurl=http://linuxsoft.cern.ch/internal/repos/iaas7-queens-stable/x86_64/os/ \nenabled=1 \ngpgcheck=0 \npriority=1 \n' \
    > /etc/yum.repos.d/cci7-iaas-queens.repo
RUN printf '[centos7-cloud-openstack-queens] \nname=Openstack RDO \nbaseurl=http://linuxsoft.cern.ch/centos/7/cloud/x86_64/openstack-queens \nenabled=1 \ngpgcheck=0 \npriority=1 \n' \
    > /etc/yum.repos.d/centos7-cloud-openstack-queens.repo
RUN printf '[cci7-openstack-clients] \nname=Cern rebuilds for Openstack clients \nbaseurl=http://linuxsoft.cern.ch/internal/repos/openstackclients7-queens-stable/x86_64/os/ \nenabled=1 \ngpgcheck=0 \npriority=1 \n' \
    > /etc/yum.repos.d/cci7-openstack-clients.repo
RUN printf '[cci7-utils] \nname=Cloud Utils repository \nbaseurl=http://linuxsoft.cern.ch/internal/repos/cci7-utils-stable/x86_64/os/ \nenabled=1 \ngpgcheck=0 \npriority=1 \n' \
    > /etc/yum.repos.d/cci7-utils.repo

# One should use either protect or priorities, but never both
RUN sed -i s/protect=1/protect=0/g /etc/yum.repos.d/*

# Tools for package building
RUN yum install -y \
    koji \
    rpm-build \
    rpmdevtools \
    rpmdev-setuptree

RUN yum install -y \
    python2-pip \
    gcc \
    python-devel \
    git

# Requirement for python-ldapclient
RUN yum install -y \
    openldap-devel

RUN pip install virtualenv tox pytest===3.3.2 pbr===3.1.1

RUN yum clean all

# CERN internal koji config (for uploading the packages)
COPY koji.conf /etc/koji.conf
