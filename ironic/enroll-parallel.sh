#!/bin/env bash

# example:
# ./enroll-parallel.sh < FILE_WITH_HOSTNAMES_AND_CREDENTIALS

while read NAME IPMI_USERNAME IPMI_PASSWORD IPMI_ADDRESS
do
    echo ${NAME} ${IPMI_USERNAME} ${IPMI_PASSWORD} ${IPMI_ADDRESS}

    echo "create $NAME"
    ironic node-create \
        -d pxe_ipmitool \
        -i ipmi_port=623 \
        -i ipmi_username=$IPMI_USERNAME \
        -i ipmi_password=$IPMI_PASSWORD \
        -i ipmi_address=$IPMI_ADDRESS \
        -i deploy_kernel=d50eeaee-b64d-4f63-abe2-40e15e80791a \
        -i deploy_ramdisk=d50eeaee-b64d-4f63-abe2-40e15e80791a \
        -n=$NAME

    echo "manage $NAME"
    ironic --ironic-api-version 1.31 node-set-provision-state $NAME manage

    echo "inspect $NAME"
    ironic --ironic-api-version 1.31 node-set-provision-state $NAME inspect

done
