#!/bin/bash

# make sure you're in the correct project before running this script

# adapt these values
NUM=$1
CELL=gva_project_040
FLAVOR=p1.dl6636624.S513-V-IP54
DELIVERY=6636624
# <-- end 'adapt these values'

for i in $(seq 1 $NUM); do
    host=`mkpasswd -l 7 -d 7 -c 0 -C 0 -s 0`
    echo $host
    ai-bs -f $FLAVOR --landb-mainuser ai-openstack-admin --landb-responsible ai-openstack-admin --foreman-environment nova_prestage -g cloud_compute/level2/kvm/$CELL --cc7 i$DELIVERY$host
done
