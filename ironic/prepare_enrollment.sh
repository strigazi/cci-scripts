#/usr/bin/env sh

# convert a hostname into a (serial-asset tag, IPMI user, IPMI password, IPMI IP address) tuple

host=$1

# get username and password
up=`ai-ipmi get-creds $host 2>&1 | grep -E '(Username|Password)'| awk '{printf "%s ", $2}'`
IFS=' ' 
set $up
u=$1
p=$2

# construct the name from the asset and serial numbers
sa=`ipmitool fru -H $host-ipmi.cern.ch -U "$u" -P "$p"|grep -E '(Asset|Product Serial)'|head -n2|cut -d':' -f 2|tr -d '\n'`
IFS=' '
set $sa
s=$1
a=$2

# get the IPMI IP address
h=`host -i $host-ipmi.cern.ch|awk '{print $4}'`

# name
echo -n "$a-$s"|sed 's/ //g'|cut -d '.' -f 1|tr '[:upper:]' '[:lower:]'|tr '\n' ' '

# user
echo -n " $u"

# password
echo -n " $p"

# IPMI IP
echo " $h"
