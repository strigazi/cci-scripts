Welcome to cloud-tools!
=======================


This repository stores all the scripts, tools and templates used in the everyday work of the Cloud Infrastructure Team.

Take a quick look to this document to know a bit more about what is inside this repo.

----------

ci-mco                       : run a command on a list of hosts

cinder/virsh-throttle-rbd.py : check (and set) virsh RBD QoS thresholds

get_swapping_processes       : get a list of all processes using swap space

image_id2sha1                : convert a Glance image UUID to the sha1 sum used as the image filename in _base

landb-tools                  : a collection of script to interrogate Landb information about hosts and services,
                               and some other goodies.
