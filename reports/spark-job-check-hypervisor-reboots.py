"""Spark job to detect reboots in the hypervisors.

Author: Luis Pigueiras <luis.pigueiras@cern.ch>
"""
from __future__ import print_function

import logging
import smtplib
import time

import argparse
import datetime
from email.mime.text import MIMEText
import json
from py4j.protocol import Py4JJavaError
from pyspark import SparkContext

SPARK_JOB_NAME = "spark-job-check-hypervisor-reboots"
logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s %(levelname)s %(name)s - %(message)s")
logger = logging.getLogger("create_project")


def send_email(subject, destination, body):
    """Send email

    :param subject: the subject of the email
    :param destination: where to send the email
    :param body: the content of the email
    """
    _from = "Cloud Account for HDFS read access <svchdfs@mail.cern.ch>"
    msg = MIMEText(body, "plain")
    msg["Subject"] = subject
    msg["From"] = _from
    msg["To"] = destination

    try:
        s = smtplib.SMTP("localhost")
        s.sendmail(_from, [destination], msg.as_string())
    finally:
        s.quit()


def to_epoch(date):
    """Convert datetime to epoch

    :args date: datetime object to convert
    :returns: the date in epoch
    """
    return (date - datetime.datetime(1970, 1, 1)).total_seconds()


def safe_json_loads(json_string):
    """Parse JSON safely

    :args json_string: a string that should be a JSON
    :returns: a dict with the JSON parsed, if it cannot be parsed an empty list
    """
    try:
        return json.loads(json_string)
    except Exception:
        return []


def hdfs_path_exist(sc, path):
    """Check if HDFS exists

    :args sc: spark context
    :args path: a HDFS path to check
    :returns: True if the path exists, False otherwise
    """
    try:
        rdd = sc.textFile(path)
        rdd.take(1)
        return True
    except Py4JJavaError:
        # if there is any exception it will mean that no directory is available
        return False


def build_node_message(node, timestamps):
    """Build report message for one node

    Will generate a line per node with a list of reboots in that specific node

    :args node: the affected node
    :args timestamp: a list of timestamps that will tell when the node was
        rebooted
    :returns: a string with the message for that specific node
    """
    message = []
    message.append("=" * 50)
    message.append("{0} has been restarted {1} times at:".format(
                   node, len(timestamps)))
    # we will print the list of reboot times ordered by time
    for timestamp in sorted(timestamps, reverse=True):
        message.append("- {0}".format(time.strftime("%d %B %Y at %H:%M:%S",
                                      time.localtime(timestamp))))

    return "\n".join(message)


def build_message(result):
    """Build final message from Spark job result

    :args result: the output of the Spark job
    :returns: a string with the result of the job
    """
    # nodes with more restarts first
    result = sorted(result, key=lambda x: (len(x[1]), max(x[1])), reverse=True)

    message = [build_node_message(node, timestamps)
               for node, timestamps in result]

    return "\n".join(message)


def get_hdfs_folders_for_date(days_ago, now, sc):
    """Get existing folders in HDFS for those dates

    :param now: end of the interval
    :param days_ago: start of the interval
    :param sc: spark context
    :returns: a list of existing folders
    """
    dates = set([now.strftime("%Y-%m"), days_ago.strftime("%Y-%m")])

    # only check in cloud_compute folder
    folders = ["/project/itmon/archive/lemon/cloud_compute/{0}/*".format(date)
               for date in dates]

    # only return folders that exist in HDFS
    return [folder for folder in folders if hdfs_path_exist(sc, folder)]


if __name__ == '__main__':
    # create the spark context
    sc = SparkContext(appName=SPARK_JOB_NAME)

    # arguments to parse
    parser = argparse.ArgumentParser(
        description="Spark job to get hypervisors rebooted several times.")
    parser.add_argument("--days", required=True, type=int,
                        help="how many days ago to start searching for "
                             "rebooted hypervisors")
    parser.add_argument("--email", required=True, type=str,
                        help="destination email")
    args = parser.parse_args()

    # calculate dates
    now = datetime.datetime.now()
    days_ago = now - datetime.timedelta(days=args.days)

    # get folders where to check depending on the range specified
    existing_folders = get_hdfs_folders_for_date(days_ago, now, sc)
    logger.info("Checking the following folders: {0}"
                .format(",".join(existing_folders)))

    # real spark job
    rdd = sc.textFile(",".join(existing_folders))
    result = (rdd
              .map(safe_json_loads)
              .filter(lambda x: x)
              .filter(lambda x: x["metric_name"] == "BootTime")
              .filter(lambda x: "aggregated" not in x)
              .map(lambda x: (x["entity"], json.loads(x["body"])["BootTime"]))
              .filter(lambda x: x[1] > to_epoch(days_ago))
              .distinct()
              .map(lambda x: (x[0], [x[1]]))
              .reduceByKey(lambda a, b: a + b)
              .collect())

    # build message to print and to send an email
    message = build_message(result)

    # Subject of the email
    subject = "Hypervisor reboots during last {0} days".format(args.days)

    send_email(subject, args.email, message)
    # to see the output in the Rundeck logs if we don't want the email
    print(message)
