Windows tools
============
These are the tools we use in the CERN Cloud for Windows administration tasks.

User notification
--------------------
The user notification folder contains a set of PowerShell scripts inherited from the CVI service. They're originally used to notify users when we schedule the patching of the hypervisors.

In order to setup this set of scripts, it's recommended to copy them into your *Documents* folder on Windows, under *WindowsPowerShell\Modules*. This will make them available when you start a new PowerShell session.

    Import-Module OpenStack
    $schedule = @{};
    $schedule.add("hypervisor01","19/11/14 06:00");
    $schedule.add("hypervisor02","21/11/14 06:00");
    $VM = @("affected-vm-01", "affected-vm-02");
    Send-OpenstackNotification -ScheduleHost $s -VM $vm -Template '.\Hypervisor update affecting your Virtual Machines (#VM#).txt' -HTML -Send

It's important to test the script before you send the e-mails to the owners, just removing the *-Send* flag will execute a dry run of the script.
