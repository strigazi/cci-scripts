function Send-Mail{

	Param(
		$To,
		$Subject,
		$Content,
		$Template,
		$Replace,
		[switch]$HTML,
		[switch]$Send
	)

	$from		= "no-reply@cern.ch"	# From
	$bcc 		= "luis.fernandez.alvarez@cern.ch"  # Bcc, you can add new recipients adding them after "," ex. "user1","user2"
	$smtpServer = "cernmx.cern.ch"					# SMTP server name
	
    # create a Mail object
    $msg = New-Object Net.Mail.MailMessage

    # create SMTP server object
    $smtp = New-Object Net.Mail.SmtpClient($smtpServer)
	
	if (($Template -ne $Null) -and $(Test-Path $Template)) {
	
	# get content
		$Content = Get-Content $Template
		
		# copy line by line, this prevents join of all the text to one unreadable 
		foreach ($line in $Content) {
			$messagebody = $messagebody + $line + "`r`n"
		}
		
		$Content = $messagebody
		
		$Subject = "[CernCloud] " +  $(gci $Template | %{ $_.BaseName })
		
		if ($Replace -ne $null) {
			if ($Replace.GetType().Name -eq "Hashtable" -and $Replace.Count -gt 0) {
				foreach ($key in $Replace.keys) {
					$Subject = $Subject.Replace($key, $Replace[$key])
					$Content = $Content.Replace($key, $Replace[$key])
				}
			} else {
				Write-Host "# wrong input for Replace parameter - hashtable requaired - @{`"key`"=`"value`"}" -f red
			}
		} 
	}
	
    # e-mail structure 
    $msg.From = $from
    $To | %{ $msg.To.Add($_) } 
	$bcc | % { $msg.Bcc.Add($_) }
    $msg.subject = $Subject
    $msg.body = $Content
	if ($HTML) {
		$msg.IsBodyHTML = $true
	}

	if ($Send) {
		# send e-mail
		$smtp.Send($msg)
		Write-Host $("E-mail sent to: " + $To) -f green
		sleep(2)
	} else {
		# show e-mail
		Write-Host "`n################################################ PREVIEW ################################################`n" -f yellow -b blue
		Write-Host $("From: `t`t" + $msg.From) -f yellow 
		Write-Host $("To: `t`t" + $msg.To) -f yellow 
		Write-Host $("Bcc: `t`t" + $msg.Bcc) -f yellow 
		Write-Host $("Subject: `t" + $msg.Subject) -f yellow 
		Write-Host $("`n" + $msg.body) -f yellow
		Write-Host "`n################################################ PREVIEW ################################################`n" -f yellow -b blue
	}
}

function Send-CVINotification {

	Param( 
		$VM,
		$Credential,
		$Template,
		[switch]$Send,
		[switch]$HTML
	)

	# LanDB Module is required
	Import-Module LanDB -WarningAction SilentlyContinue
	
	if ($Credential -eq $null) {
		$Credential = Get-Credential
    }   
	
	$data = Get-CviUserInfo -VM $VM -Credential $Credential
	
	$cmd = @()
	
	$data.Keys | %{ 
		$da = @{}; 
		$da.Add("#NAME#", $data[$_]["firstname"]); 
		$da.Add("#MAIL#", $data[$_]["email"]); 
		$da.Add("#VM#", $($data[$_]["vm"].Keys -join ', '));
		$vmList = "`r`n";
		foreach ($line in $data[$_]["vm"].Keys) {
			$vmList = $vmList + $("- " + $line) + "`r`n"
		}
		$da.Add("#VMLIST#", $vmList); 
		$cmd += "Send-Mail";
		$cmd += "-To `$da[`"#MAIL#`"]";
		$cmd += "-Template `$Template";
		$cmd += "-Replace `$da"; 
		if ($Send) {$cmd += "-Send"};
		if ($HTML) {$cmd += "-HTML"};
		Invoke-Expression $($cmd -join ' ');
		$cmd = @()
	}
	
}

function Send-CVINotification2 {

	Param( 
		$VM,
		$Credential,
		$Template,
		[switch]$Send,
		[switch]$HTML
	)

	# LanDB Module is required
	Import-Module LanDB -WarningAction SilentlyContinue
	
	if ($Credential -eq $null) {
		$Credential = Get-Credential
    }   
	
	$data = Get-CviUserInfo -VM $VM -Credential $Credential
	
	$cmd = @()
	
	$data.Keys | %{ 
		$da = @{}; 
		$da.Add("#NAME#", $data[$_]["firstname"]); 
		$da.Add("#MAIL#", $data[$_]["email"]); 
		$da.Add("#VM#", $($data[$_]["vm"].Keys -join ', '));
		$vmList = "`r`n";
        $vmList += "`t" + "Virtual Machine".PadRight(24, " ") + "Intervention date`n"
		$vmList += "`t" + "=".PadRight(22, "=") + "  " + "=".PadRight(22, "=") + "`n"
		foreach ($vmName in $data[$_]["vm"].Keys) {
            $vmList = $vmList + $("`t" + $vmName.PadRight(24, " ") + "data") + "`r`n"
		}
		$da.Add("#VMLIST#", $vmList); 
		$vmCnt = $data[$_]["vm"].Keys | measure | %{ $_.Count }
		$da.Add("#VMCNT#", $vmCnt);
		$cmd += "Send-Mail";
		$cmd += "-To `$da[`"#MAIL#`"]";
		$cmd += "-Template `$Template";
		$cmd += "-Replace `$da"; 
		if ($Send) {$cmd += "-Send"};
		if ($HTML) {$cmd += "-HTML"};
		Invoke-Expression $($cmd -join ' ');
		$cmd = @()
	}
	
}
