#! /usr/bin/perl -w

sub BEGIN {
    use Cwd 'abs_path';
    use File::Basename 'dirname';
    push(@INC,dirname(abs_path($0)));
}

use strict;
use diagnostics;
use SOAP::Lite;# +trace => 'debug';
use Data::Dumper;
use Text::TabularDisplay;
use Getopt::Long;
use Landb::Tools;

sub HostGroupData(@);

my $debug = my $help = my $printhostlist = undef;
my $username = getlogin();
my $verbose = 0;
my %opts = (debug   => \$debug,
	    verbose => \$verbose,
            help    => \$help,
    );
my $options = GetOptions(\%opts,
                         "debug"           => \$debug,
                         "help"            => \$help,
                         "verbose+"        => \$verbose,
			 "cern-user=s"     => \$username,
			 "print-host-list" => \$printhostlist,
    );


#
#  Go for it!
############## https://network.cern.ch/sc/soap/4/description.html
#

my $password = Landb::Tools::get_credentials($username);

my $client = SOAP::Lite                                            
    -> uri('http://network.cern.ch/NetworkService')
    -> proxy('https://network.cern.ch/sc/soap/soap.fcgi?v=x');

my $call = $client->getAuthToken($username,$password,'CERN');

my $result = $call->result;
if ($call->fault) {
    die "FAILED: " . $call->faultstring;
}

my $auth  = SOAP::Header->name('Auth' => { "token" => $result });

my @service = map {uc} @ARGV;

#exit;

#
#
#
my @header = ("Primary Service","IP addresses\nTot/Used/Pct","Network\nDomain","Routed","Description");
push(@header,"Hostgroup",) if $verbose > 1;
push(@header,"Host Names") if $verbose;
my $table = Text::TabularDisplay->new(@header);

my @hostlist = ();

for my $service (sort map {uc} keys %{{map{$_=>1} @service}}){

    $call = $client->getServiceInfo($auth,$service);

    $result = $call->result;
    if ($call->fault) {
	print STDERR "[W] Ignoring unknown service $service: " . $call->faultstring ."\n";
	next;
    }
    print "[D] " .  Dumper($result) if $debug;
    my $NetworkDomain = $result->{NetworkDomain};
    my $AddressIni    = ($result->{AddressIni} =~ /^10\./ ? "No" : "Yes");
    my $Description   = $result->{Description} || "";
    my $UserIPTotal = $result->{UserIPTotal};
    my $UserIPFree  = $result->{UserIPFree};
    my $Pct = $UserIPTotal == 0 ? 0 : 100 * ($UserIPTotal - $UserIPFree) / $UserIPTotal;

    my @data = ($service,
		sprintf("%3d/%3d/%3d%%",$UserIPTotal, $UserIPTotal - $UserIPFree,$Pct),
		$NetworkDomain,
		$AddressIni,
		$Description
	);

    if ($verbose){
	$call = $client->getDevicesFromService($auth,$service);
	$result = $call->result;
	my @server = sort map {lc} @$result;
	push(@hostlist,@server);
	if ($verbose > 1){
	    my %hg = HostGroupData(@server);
	    #print Dumper(\%hg);#exit;
	    my $HostGroup = my $HostNames = "";
	    for my $hgname (sort keys %hg){
		$HostGroup .= $hgname;
		my @server = sort @{$hg{$hgname}};
		while (my @tmp = splice(@server,0,5)){
		    last if not @tmp;
		    map {$HostNames .= sprintf("%15s ",$_)} @tmp;
		    $HostNames .= "\n";
		    $HostGroup .= "\n";
		}
	    }
	    push(@data,$HostGroup,$HostNames);
	}elsif ($verbose == 1){
	    my $HostNames = "";
	    while (my @tmp = splice(@server,0,5)){
		last if not @tmp;
		map {$HostNames .= sprintf("%15s ",$_)} @tmp;
		$HostNames .= "\n";
	    }
	    push(@data,$HostNames);
	}
    }

    $table->add(@data);

}

print $table->render . "\n";

if ($printhostlist){
    print STDOUT "[INFO] List of ".scalar(@hostlist)." hosts: ".join(" ",sort @hostlist)."\n";
}

exit;


#
# get the hostgroups from PuppetDB - pretty damn ugly :)
#
sub HostGroupData(@){
    my @host = @_;
    #print ">> HG @host\n";
    if (not open(PDB, "/usr/bin/ai-pdb raw /v2/facts/hostgroup 2>/dev/null | ")){
	print STDERR "[ERROR] \"/usr/bin/ai-pdb raw /v2/facts/hostgroup\" failed: $!\n";
	return;
    }

    my %hostgroup = ();
    my %notfound = ();
    map {$notfound{$_} = 1} @host;
    
    while(<PDB>){
	if (/^\s+\"certname\":\s+\"(\S+).cern.ch\"/){
	    my $host = $1;
	    if (not gethostbyname($host)){
		#print STDERR "[WARN] No DNS entry for PuppetDB host \"$host\", ignoring\n";
		next;
	    }
	    next unless grep {$host eq $_} @host;
	    $_ = <PDB>;
	    $_ = <PDB>;
	    if (/^\s+\"value\":\s+\"(\S+)\"/){
		#my $hostgroup = $1;
		#print " >>  $host     $hostgroup\n";
		push(@{$hostgroup{$1}},$host);
		delete $notfound{$host};
	    }
	}	    
    }
    close(PDB);

    @{$hostgroup{"(unknown)"}} = sort keys %notfound if %notfound;
    return %hostgroup;
}

__END__
