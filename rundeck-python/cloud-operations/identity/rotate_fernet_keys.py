#!/usr/bin/env python

import prettytable
import time
import sys
import logging

from argparse import ArgumentParser
from aitools.trustedbag import TrustedBagClient
from ConfigParser import ConfigParser
from cryptography.fernet import Fernet

TEIGI_TIMEOUT = 1800
TEIGI_RETRY_INTERVAL = 15

# configure logging
logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s %(levelname)s %(name)s - %(message)s")
logger = logging.getLogger("rotate_fernet_keys")


def get_teigi_key(args, index, tbagclient):
    hostgroup = args.hostgroup
    teigi_key = "%s_%s" % (args.teigi_tag, index)
    timeout = time.time() + TEIGI_TIMEOUT
    logger.info("Getting '%s' key from TEIGI..." % teigi_key)
    while time.time() < timeout:
        try:
            return tbagclient.get_key(hostgroup, "hostgroup",
                                      teigi_key)['secret']
        except Exception, ex:
            logger.error("Cannot connect to TEIGI ('%s'). "
                         "Waiting %s seconds before retrying..."
                         % (ex, TEIGI_RETRY_INTERVAL))
            time.sleep(TEIGI_RETRY_INTERVAL)

    error_msg = "Impossible to get '%s' from TEIGI after %s " \
                "seconds waiting" % (teigi_key, TEIGI_TIMEOUT)
    raise Exception(error_msg)


def update_teigi_key(args, index, value, tbagclient):
    hostgroup = args.hostgroup
    teigi_key = "%s_%s" % (args.teigi_tag, index)
    timeout = time.time() + TEIGI_TIMEOUT
    logger.info("Updating '%s' key on TEIGI..." % teigi_key)
    while time.time() < timeout:
        try:
            return tbagclient.add_key(hostgroup, "hostgroup", teigi_key, value)
        except Exception, ex:
            logger.error("Cannot connect to TEIGI ('%s'). "
                         "Waiting %s seconds before retrying..."
                         % (ex, TEIGI_RETRY_INTERVAL))
            time.sleep(TEIGI_RETRY_INTERVAL)

    error_msg = "Impossible to UPDATE '%s' TEIGI after %s " \
                "seconds waiting" % (teigi_key, TEIGI_TIMEOUT)
    raise Exception(error_msg)


def main_rotate_tokens(args, tbagclient):
    keys = args.keys
    table = prettytable.PrettyTable(["teigi_secret_tag", "Old", "New"])
    old_values = [0] * (keys + 1)
    new_values = [0] * (keys + 1)

    for i in range(0, keys + 1):
        old_values[i] = get_teigi_key(args, i, tbagclient)

    for i in range(1, keys):
        new_values[i] = old_values[i + 1]

    new_values[0] = Fernet.generate_key()
    new_values[keys] = old_values[0]

    for i in range(0, keys + 1):
        update_teigi_key(args, i, new_values[i], tbagclient)
        tag = "%s_%s" % (args.teigi_tag, i)
        table.add_row([tag, old_values[i], new_values[i]])

    # print(table)


def main():
    parser = ArgumentParser(description="Rotate keystone "
                                        "fernet tokens on Teigi")
    parser.add_argument("--hostgroup", required=True, help="Hostgroup")
    parser.add_argument("--keys", type=int, default=6,
                        help="Number of fernet keys")
    parser.add_argument("--teigi-tag", help="Teigi tag. Ex: keystone_fernet_X")

    args = parser.parse_args()
    config = ConfigParser()
    config.readfp(open('/etc/ai/ai.conf'))
    tbagclient = TrustedBagClient(host=config.get("tbag", "tbag_hostname"),
                                  port=config.get("tbag", "tbag_port"),
                                  timeout=config.get("tbag", "tbag_timeout"))

    main_rotate_tokens(args, tbagclient)


if __name__ == "__main__":
    try:
        main()
    except Exception, e:
        logger.error(e)
        sys.exit(-1)
